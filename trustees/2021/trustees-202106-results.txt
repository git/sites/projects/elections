_reopen_nominations
alicef
antarus
robbat2

                     _reop  alice  antar  robba
_reopen_nominations    ***     16      7      0
             alicef     16    ***      3      1
            antarus     25     24    ***      4
            robbat2     32     27     20    ***

option _reopen_nominations is eliminated (antarus trans-defeats _reopen_nominations, and _reopen_nominations does not trans-defeat antarus)
option alicef is eliminated (antarus trans-defeats alicef, and alicef does not trans-defeat antarus)
option antarus is eliminated (robbat2 trans-defeats antarus, and antarus does not trans-defeat robbat2)
the Schwartz set is {robbat2}

result: option robbat2 wins

*** Running another pass to find the next winners... ***

                     _reop  alice  antar  robba
_reopen_nominations    ***     16      7      0
             alicef     16    ***      3      1
            antarus     25     24    ***      4
            robbat2     -1     -1     -1    +++

option _reopen_nominations is eliminated (antarus trans-defeats _reopen_nominations, and _reopen_nominations does not trans-defeat antarus)
option alicef is eliminated (antarus trans-defeats alicef, and alicef does not trans-defeat antarus)
the Schwartz set is {antarus}

result: option antarus wins

*** Running another pass to find the next winners... ***

                     _reop  alice  antar  robba
_reopen_nominations    ***     16      7      0
             alicef     16    ***      3      1
            antarus     -1     -1    +++     -1
            robbat2     -1     -1     -1    +++

the Schwartz set is {_reopen_nominations, alicef}

result: tie between options _reopen_nominations, alicef

*** Finished ranking candidates ***

Final ranked list:
robbat2
antarus
_reopen_nominations alicef
